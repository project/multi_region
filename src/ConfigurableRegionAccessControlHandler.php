<?php

namespace Drupal\multi_region;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Configurable Region entity.
 */
class ConfigurableRegionAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'update':
        if ($account->hasPermission('edit configurable region entities')) {
          return AccessResult::allowed();
        }
        break;

      case 'delete':
        if ($account->hasPermission('delete configurable region entities')) {
          return AccessResult::allowed();
        }
        break;

    }
    return AccessResult::forbidden();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'create configurable region entities');
  }

}
